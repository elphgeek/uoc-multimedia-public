/*
  Kobo (Book)
 
 Apaga i enciende un led a partir de las lecturas de un sensor de luminosidad.
 El circuito esta pensado para colocarse en el interior de un libro. La apertura
 i cierre de la tapa hace que el Arduino entre en modo de ahorro de energia.
 El circuito esta pensado para trabajar con 6 pilas AA de 1.5V
 
 Creado 14/04/2013
 por Jordi Martínez Bosch <http://elphnet.com>
 
 */

// Incluimos librerias para el modo sleep
#include <avr/interrupt.h>
#include <avr/power.h>
#include <avr/sleep.h>
#include <avr/io.h>

// === Definimos constantes
// maximo valor pwm para el led
const int MAX = 255;
// Umbral de luminosidad / oscuridad captada por el LDR
// para decidir si encendemos o apagamos
const int THRESHOLD = 950;
// tiempo de espera entre consultas (ms)
const int TIME_WAIT = 1000;

// === Definimos las entradas i salidas
const int LED = 5; 
const int LDR = 3;
// Boton en el pin2, pues las interrupciones por hardware son en los pines 2/3
const int BTN = 2;

// === Variables
// booleano para saber si el libro esta abierto o cerrado
boolean isOpen = false;
// booleano para saber si estamos encendidos o apagados
boolean isOn = false;
// Cantidad de luz actual en el led
int currentLight = 0;

/**
 *  Inicializacion
 *    Establecemos los pines de entrada i salida i la Salida por el puerto serial
 */
void setup() 
{
  // Abrimos la comunicacion serial, para obtener las lecturas (trace) 
  //Serial.begin(9600);
  // Definimos los pines i si son de entrada o salida
  pinMode(LED, OUTPUT);
  pinMode(LDR, INPUT);
  pinMode(BTN, INPUT);   

  // Serial logging
  Serial.begin(9600);
  Serial.println("Inicialización completa");
}

/**
 *  Bucle de ejecución
 *    Cada iteración comprovaremos el entorno i actuaremos en consecuencia. Despues efectuamos una pausa.
 *    Si despues de la pausa, tenemos el libro cerrado i el led apagado (el apagado de leds no es inmediato, 
 *    pues hacemos un fade) ponemos la placa en modo durmiente para ahorrar energia.
 */
void loop() 
{
  checkEnvironment();
  delay(TIME_WAIT);

  // el libro está cerrado i los leds apagados
  if(!isOpen && !isOn)
  {
    Serial.println("Sleeping");
    enterSleep(); 
  }
} 

/**
 *  Comprueba el entorno i actua en consecuencia
 *    1.- Comprueba el estado del boton: 
 *      pulsado (HIGH) = tapa cerrada, apagamos led i flag de cerrado
 *      sin pulsar (LOW) = tapa abierta, flag de abiero
 *    2.- (Solo si abierto) Comprueba el sensor de luminosidad (LDR) para ver si ha traspasado el umbral
 *        2.1.- Si supera el umbral i no esta encendido, lo encendemos
 *        2.2.- Si no supera el umbral i esta encendido, lo apagamos
 */
void checkEnvironment(){
  Serial.println("Check environment");

  // obtenemos el estado del boton, cuando este apretado, es porque la tapa esta cerrada, 
  int state = digitalRead(BTN);
  if(state == HIGH){
    // apagamos el led, si estuviera encendido
    if(isOn){ 
      ledOff();
    }    
    isOpen = false;
  } 
  else {
    isOpen = true;
  }

  // Comprobamos el estado de la tapa antes de hacer nada. 
  // Si esta cerrado no hacemos nada
  if(!isOpen) return;

  // Leemos el valor del sensor
  int val = analogRead(LDR);
  Serial.print("LDR:");
  Serial.println(val);

  // Comprovamos si tenemos que encender o apagar el led
  if(val >= THRESHOLD && !isOn){
    Serial.println("  > ON");
    ledOn(); 
  } 
  else if(val < THRESHOLD && isOn){
    Serial.println("  > OFF");
    ledOff();
  }  
}

/**
 *  Enciende los leds
 *    Enciende los leds via PWM de forma gradual
 */
void ledOn(){
  isOn = true;
  for(int light = 0; light <= MAX; light += 5) 
  {
    analogWrite(LED, light);
    delay(100);
    currentLight = light;
  }
}

/**
 *  Apaga los leds
 *    Apaga los leds via PWM de forma gradual
 */
void ledOff(){
  isOn = false;
  for(int light = MAX; light >= 0; light -= 5) 
  {
    analogWrite(LED, light);
    delay(100);
    currentLight = light;
  }
}

/*
 *  Activamos el modo SLEEP para ahorrar energia
 */
void enterSleep(void)
{
  // Establecemos el pin2 como interruptor i lo asociamos al metodo pin2Interrupt
  attachInterrupt(0, pin2Interrupt, LOW);
  delay(100);

  // Activamos el modo SLEEP
  set_sleep_mode(SLEEP_MODE_PWR_DOWN);
  sleep_enable();
  sleep_mode();

  // === Cuando se despierte la ejecucion continuara aqui 
  // Desactivamos el modo SLEEP
  sleep_disable(); 
}

/*
 *  Interrupción hardware al pulsar el boton enlazado al pin2
 */
void pin2Interrupt(void)
{
  // Desasociamos la interrupcion para evitar que se siga lanzando cuando el pin2 este en LOW
  detachInterrupt(0);
}



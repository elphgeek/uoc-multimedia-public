
PVector _attractor;
ArrayList<Particle> particles = new ArrayList<Particle>();

void setup() {
  size(600, 600, P2D);
  smooth();
}

void draw() {
  background(26, 26, 26);
  for (Particle p : particles)
  {
    p.update(_attractor);
    p.draw();
  }

  if (_attractor!=null) {
    noStroke();
    fill(117, 225, 216, 20);
    ellipse(_attractor.x, _attractor.y, 120, 120);
    fill(117, 225, 216, 60);
    ellipse(_attractor.x, _attractor.y, 40, 40);
    fill(117, 225, 216);
    ellipse(_attractor.x, _attractor.y, 4, 4);
  }

  fill(255);
  text("+ per a afegir particules (" + particles.size() +")", 10, height-60);
  text("- per a eliminar particules", 10, height-40);
  text("Click per a crear un vortex", 10, height-20);
}

void keyPressed() {
  switch ( key) {
  case '+':
    particles.add(new Particle(new PVector(random(width), random(height))));
    break;
  case '-':
    if (particles.size()> 0) {
      particles.remove(particles.size()-1);
    }
    break;
  }
}

void mousePressed() {
  _attractor = new PVector(mouseX, mouseY);
}


class Particle {
  PVector _position;
  PVector _velocity;
  PVector _acceleration;

  public Particle(PVector position) {
    _position = position;
    _velocity = new PVector(0, 0);
  } 

  public void draw() {
    noStroke();
    fill(255);
    ellipse(this._position.x, this._position.y, 3, 3);
  }

  public void  update(PVector attractor) {
    if (attractor!=null) {
      PVector dir = PVector.sub(attractor, _position);
      dir.normalize();
      dir.mult(0.5);
      _acceleration = dir;

      _velocity.add(_acceleration);
      _velocity.limit(10);
      _position.add(_velocity);
    }
  }
}


import java.util.*;

// Escena. Representa un terreno que varia dependiendo de las variables
public class Waves extends Scene {

  int i, x, y, s = 10;
  float k, m, r, j = .01f, vol = .001f;
  int MAX;

  Rain rain;
  ArrayList<Tile> tiles = new ArrayList<Tile>();
  ArrayList<Float> volHistory = new ArrayList<Float>();

  public Waves(World world) {
    super(world);
    MAX = displayWidth * displayHeight;
  }

  public void draw() {
    super.world.draw();

    // Obtenemos las variables del mundo
    Variables vars = super.world.getVariables();

    // Pintamos el fondo
    background( vars.getBgColor());

    // Esta lloviendo? si es asi pintamos la lluvia
    if (vars.getIsRaining()) {
      if (rain == null) {
        rain = new Rain(vars);
      }
      rain.update(vars);
      rain.draw(vars);
    }  

    // Inicializamos los cuadrados, si no tenemos
    if (this.tiles.size() == 0) {
      for (i = 0; i < MAX; i += s) {
        this.tiles.add(new Tile(vars.getRandomColor())); 
        if (i % displayWidth == 0) {
          i +=  displayWidth * (s - 1);
        }
      }
    }

    // Annalisis de audio: Volumen i Beat
    boolean isBeat = vars.getBeat().isRange(2, 15, 2);
    boolean isKick = vars.getBeat().isKick();
    boolean isSnare = vars.getBeat().isSnare();
    boolean isHat = vars.getBeat().isHat();
    float volumen = (float)map(vars.getVolumen(), 0, 255, .0005, .01);

    // calculamos la media de los 5 ultimos volúmenes  
    float avg= 0;
    for (int i = 0; i < volHistory.size(); i++) {
      avg += volHistory.get(i);
    }
    avg /= volHistory.size();
    if (volHistory.size() > 5) {
      volHistory.remove(0);
    }
    volHistory.add(volumen);

    // y usamos la media calculada para el noise que nos genera el terreno
    vol = avg;
   
    // Si la media es demasiado baja, usamos un valor minimo
    // para que el terreno ondule
    if (vol < 0.0001)
      vol = 0.0001;

    // Sin iluminacion
    noLights();
    //directionalLight(126, 126, 126, 0, 0, -1);
    //directionalLight(255, 255, 255, 0, -1, -1);

    // Pintamos el terreno
    beginShape(TRIANGLE);
    int index = 0;
    
    // iteramos hasta ancho * alto en incrementos de s (10);
    for (int i = 0; i < MAX; i += s) {

      // la X será el resto de i / ancho (si i = 100 i ancho = 100 > x = 0
      x = i % displayWidth;   
      y = i / displayWidth;  // punt y = index / ample
      k = y + s;        // k = y + separacio
      m = x + s;        // m = x + separacio

      // Obtenemos el tile 
      Tile t = this.tiles.get(index);

      //cambiamos la opacidad si hay un beat, si no subimos o bajamos la opacidad dependiendo si esta muriendose o no
      if (t.alpha < 80 && ( isBeat )) {
        t.alpha = 255;
      }
      else {
        t.alpha += t.dying ? -random(0, 15): random(0, 10);
      }

      // Actualizamos el tile i lo pintamos
      t.update(x, y, k, m, r, vol, vars);
      t.draw();

      // Gestion de tiles muertos i una vez opacos, cambiamos el color por otro
      if (t.alpha >= 150 && !t.dying) t.dying = true;
      if (t.alpha <= 10) {
        t.dying = false;
        t.c = vars.getRandomColor();
      }

      index++;
      i += (i % displayWidth == 0) ?  displayWidth * (s - 1) : 0;
    }
    endShape();
    r -= j;
  }
}

// Clase que representa un cuadrado de terreno
class Tile {
  public PVector[] points;
  public color c;
  public float alpha = 0;
  public boolean dying = false;
  public float _r;

  // crea una nueva instancia de cuadrado, con un color concreto
  public Tile(color c) {
    this.c = c;
    // Cada cuadrado tiene 6 puntos, los inicializamos a cero
    this.points = new PVector[6];
    for (int i = 0; i<6;i++) {
      this.points[i] = new PVector(0, 0, 0);
    }
  }

  // Actualizamos los puntos del Tile
  public void update(float x, float y, float k, float m, float r, float v, Variables vars) {
    _r = r;
    float a = y * displayWidth;
    float b = k * displayWidth;

    this.points[0] = new PVector(x, n(a + x, v), y);
    this.points[1] = new PVector(m, n(a + m, v), y);
    this.points[2] = new PVector(m, n(b + m, v), k);
    this.points[3] = new PVector(m, n(b + m, v), k);
    this.points[4] = new PVector(x, n(b + x, v), k);
    this.points[5] = new PVector(x, n(a + x, v), y);
  }

  // Dibujamos el Tile
  public void draw() {
    // linia semitransparente blanca
    stroke(255, 255, 255, 60);
    // relleno con el color i la opacidad
    fill(this.c, this.alpha);
    // Dibujamos la forma 
    for (int i = 0; i<6;i++) {
      vertex(this.points[i].x, this.points[i].y, this.points[i].z);
    }
  }

  // genera un pseudoaleatorio basado en noise
  float n(float i, float vol) {
    float x = i % displayWidth * vol;
    float y = i * vol / displayWidth + _r;
    return noise(x, y) * 400 + displayHeight / 2;
  }
}


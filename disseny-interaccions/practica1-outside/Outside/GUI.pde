// Interfaz de depuracion
//    permite al usuario modificar los valores de las variables de forma sencilla i visula
class GUI implements controlP5.ControlListener {

  World _world;
  ControlP5 _gui;
  Println console;
  ControlGroup group; 

  color textColor = color(255);
  color bgColor = color(60);
  color activeColor = color(0, 160, 176);

  CheckBox _override;
  DropdownList _dropSeasons;
  Knob _volumen;
  Bang _beat;
  Knob _hour;

  Knob _rain;
  Slider _ws;
  Slider _wd;

  // Inicializa los controles para las variables
  public GUI(PApplet applet, World world) {
    _world = world;

    _gui = new ControlP5(applet);
    _gui.addListener(this);
    _gui.enableShortcuts();

    int startX = 0;
    int startY = 0;
    int guiWidth = 250;

    // Contenedor de controles
    group = _gui.addGroup("debugWindow", startX, startY, guiWidth);
    group.setBackgroundHeight(displayHeight);
    group.setBackgroundColor(color(0, 60));
    group.hideBar();

    // Frame rate
    _gui.addFrameRate()
      .setInterval(10)
        .setPosition(guiWidth-30, 10)
          .moveTo(group);

    startX += 20;
    startY += 10;

    // Titulo
    _gui.addTextlabel("mainTitle")
      .setText("DEBUG MODE: ON ( push 'd' to exit debug mode )")
        .setPosition(startX, startY)
          .moveTo(group);

    startY += 50;

    // Seleccion de estacion del año
    _dropSeasons = _gui.addDropdownList("dropSeasons")
      .setId(1)
        .setPosition(startX, startY)
          .setSize(200, 200);

    _dropSeasons.setBackgroundColor(color(190));
    _dropSeasons.setItemHeight(20);
    _dropSeasons.setBarHeight(15);
    _dropSeasons.captionLabel().set("Season");
    _dropSeasons.captionLabel().style().marginTop = 3;
    _dropSeasons.captionLabel().style().marginLeft = 3;
    _dropSeasons.valueLabel().style().marginTop = 3;
    _dropSeasons.setColorBackground(bgColor);
    _dropSeasons.setColorActive(activeColor);

    _dropSeasons.addItem("Winter", 0);
    _dropSeasons.addItem("Spring", 1);
    _dropSeasons.addItem("Summer", 2);
    _dropSeasons.addItem("Autum", 3);

    startY += 20;

    // Volumen
    _volumen = _gui.addKnob("knobVolumen")
      .setId(2)
        .setRange(0, 255)
          .setValue(0)
            .setPosition(startX, startY)
              .setRadius(20)
                .setDragDirection(Knob.HORIZONTAL)
                  .moveTo(group);
    _volumen.captionLabel().set("Volume");

    // Beat, cada clic simulara un evento de Beat
    _beat = _gui.addBang("bangBeat")
      .setPosition(startX + 50, startY)
        .setSize(40, 40)
          .setLabel("Beat")
            .moveTo(group)
              .setId(4);

    startY += 80;

    // Hora del dia
    _hour = _gui.addKnob("knobHour")
      .setId(3)
        .setRange(0, 23)
          .setValue(0)
            .setPosition(startX, startY)
              .setRadius(20)
                .setDragDirection(Knob.HORIZONTAL)
                  .setNumberOfTickMarks(24)
                    .setTickMarkLength(4)
                      .snapToTickMarks(true)
                        .moveTo(group);
    _hour.captionLabel().set("Hour");

    startY += 80;

    // Nivel de lluvia
    _rain = _gui.addKnob("knobRain")
      .setId(5)
        .setRange(0, 5)
          .setValue(0)
            .setPosition(startX, startY)
              .setRadius(20)
                .setDragDirection(Knob.HORIZONTAL)
                  .setNumberOfTickMarks(5)
                    .setTickMarkLength(4)
                      .snapToTickMarks(true)
                        .moveTo(group);
    _rain.captionLabel().set("Rain Level");

    startY += 80;

    // Velocidad del viento
    _ws = _gui.addSlider("Wind Speed")
      .setPosition(startX, startY)
        .setRange(0, 50)
          .setId(6)
            .moveTo(group);

    startY += 30;

    // Direccion del viento (grados)
    _wd = _gui.addSlider("Wind Direction")
      .setPosition(startX, startY)
        .setRange(0, 360)
          .setId(7)
            .moveTo(group);

    startY += 30;

    // Yahoo weather attribution terms of use
    // The feeds are provided free of charge for use by individuals and non-profit organizations for personal, 
    // non-commercial uses. We ask that you provide attribution to Yahoo! Weather in connection with your use of the feeds.

    _gui.addTextlabel("yahooAttr")
      .setText("Powered by Yahoo! Weather")
        .setPosition(startX, displayHeight-35)
          .moveTo(group);


    // Para mantenerlo encima de los demas
    _dropSeasons.moveTo(group);

    setDisplay(false);
  }

  // Cambia el estado de la interfaz de visible a oculto i viceversa
  public void setDisplay(boolean debug) {
    println("DEBUG MODE = " + (debug? "ON" : "OFF"));
    // Le decimos al Mundo que estamos en modo depuracion
    _world.setDebugMode(debug);
    if (debug)group.show();
    else group.hide();
  }


  // Control de eventos
  void controlEvent(ControlEvent e) {

    // DropdownList is of type ControlGroup.
    // A controlEvent will be triggered from inside the ControlGroup class.
    // therefore you need to check the originator of the Event with
    // if (theEvent.isGroup())
    // to avoid an error message thrown by controlP5.

    // Obtenemos las variables del Mundo
    Variables _vars = _world.getVariables();

    // Modificamos los valores de las variables si se ha producido un cambio en los controles
    if (e.isGroup()) {
      switch(e.getGroup().getId()) {
      case 1:
        _vars.setSeason((int)_dropSeasons.getValue());
        break;
      }
      println("got a control event from controller with id "+e.getGroup().getId());
      println("event from group : "+e.getGroup().getValue()+" from "+e.getGroup());
    } 
    else if (e.isController()) {
      println("got a control event from controller with id "+e.getController().getId());
      println("event from controller : "+e.getController().getValue()+" from "+e.getController());

      // Volumen
      if (e.getController().getId() == 2) {
        _vars.setVolumen((int)e.getController().getValue());
      }
      // Hora del dia
      if (e.getController().getId() == 3) {
        _vars.setHourOfDay((int)e.getController().getValue());
      }
      // Beat
      if (e.getController().getId() == 4) {
        _vars.setDebugBeat(true);
      }

      // Nivel de lluvia:
      //  Establecemos un codigo de condicion meteorologica del nivel seleccionado
      if (e.getController().getId() == 5) {
        int value = (int)e.getController().getValue();
        int code = 32; // sunny;
        switch(value) {
        case 1:
          code = 9; // drizzle
          break; 
        case 2:
          code = 12; // showers
          break;
        case 3:
          code = 5; // mixed rain and snow
          break;
        case 4:
          code = 45; // thundershowers
          break;
        case 5:
          code = 2; // hurricane
          break;
        }

        _vars.setWeather(code, -1, -1, -1);
      }

      // Velocidad del viento
      if (e.getController().getId() == 6) {
        _vars.setWeather(-1, -1, (float)e.getController().getValue(), -1);
      }

      // Direccion del viento
      if (e.getController().getId() == 7) {
        _vars.setWeather(-1, -1, -1, (int)e.getController().getValue());
      }
    }
  }
}


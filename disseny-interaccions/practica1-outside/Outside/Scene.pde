// Clase base para las escenas
abstract class Scene {

  // Todas las escenas tienen acceso al Mundo
  protected World world;
  
  // Constructor
  public Scene(World w) {
    this.world = w;
  }
  
  // Todas las escenas deben implementar el metodo draw
  abstract void draw();
}


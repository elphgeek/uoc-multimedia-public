import ddf.minim.spi.*;
import ddf.minim.signals.*;
import ddf.minim.*;
import ddf.minim.analysis.*;
import ddf.minim.ugens.*;
import ddf.minim.effects.*;
import controlP5.*;
import java.util.*;


// Identificador de "Where On Earth ID" para Yahoo Weather
//   Se puede encontrar en http://woeid.rosselliot.co.nz/
int WOEID = 766465;

GUI _gui;
Scene _scene;
World _world;
boolean debug = false;

void setup() {
  size(displayWidth, displayHeight, OPENGL);
  smooth();
  frameRate(24);

  // Instanciamos el Mundo, la Interfaz de depuración y la escena principal
  _world = new World(this, WOEID);
  _gui = new GUI(this, _world);
  _scene = new Waves(_world);
}

// Dibujamos la escena
//    las llamadas a DEPTH_TEST son para que la interfaz de 
//    depuracion se ponga por encima de la escena, aunque sea tridimensional
void draw() {
  hint(ENABLE_DEPTH_TEST);
  pushMatrix();
  _scene.draw();
  popMatrix();
  hint(DISABLE_DEPTH_TEST);
}

// Paramos el Mundo, principalmente para finalizar correctamente Minim
void stop() {
  _world.stop();
}

// Eventos de teclado
//    d = Activar desactivar modo de depuracion
//    p = Guardar captura
//    1 = Cambiar a escena 1 WAVES (principal)
//    2 = cambiar a escena 2 TRIANGLES
void keyPressed() {
  if (key == 'd') {
    debug = !debug;
    _gui.setDisplay(debug);
  }
  if (key == 'p') saveScreen();
  if (key == '1') _scene = new Waves(_world);
  if (key == '2') _scene = new Triangles(_world);
}

// Guardar una captura en formato PNG
void saveScreen() {
  String timestamp = year() + nf(month(), 2) + nf(day(), 2) + "-"  + nf(hour(), 2) + nf(minute(), 2) + nf(second(), 2);
  save(timestamp + ".png");
}


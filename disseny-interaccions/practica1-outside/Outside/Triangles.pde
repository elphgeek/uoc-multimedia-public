
// Escena. Representa las variables como un visual bidimensional
class Triangles extends Scene {

  int w, h ;
  ArrayList<Triangle> triangles = new ArrayList<Triangle>();
  Rain rain;

  // crea una nueva instancia de la escena
  public Triangles(World world) {
    super(world);
    w = displayWidth;
    h = displayHeight;

    // No 3d on this visualization
    hint(DISABLE_DEPTH_TEST);
  }

  // Dibujamos la escena
  public void draw() {
    // actualizamos el mundo
    super.world.draw();

    // obtenemos las variables
    Variables vars = super.world.getVariables();

    // Pintamos el fondo
    background(vars.getBgColor());

    // Si llueve pintamos la lluvia
    if (vars.getIsRaining()) {
      if (rain == null) {
        rain = new Rain(vars);
      }
      rain.update(vars);
      rain.draw(vars);
    }

    int _moveX = 0;
    int _moveY = 0;

    // Analisis de sonido
    int v = (int)map(vars.getVolumen(), 0, 255, 1, 100);

    // Si detectamos un beat, movemos algunos de los triangulos ligeramente hacia arriba
    if (vars.getBeat().isRange(2, 15, 2)) {
      _moveY += -2;
    }

    // Como mas alto el volumen mayor la cantidad de triangulos
    for (int i = 0; i<v; i++) {
      Triangle t = new Triangle();
      t.setColor(vars.getRandomColor());
      triangles.add(t);
    }   

    // Actualizamos i pintamos los triangulos
    for (int i = 0; i<triangles.size(); i++) {
      Triangle t = triangles.get(i);
      // Movemos los triangulos si hay beat
      if (i%2 == 0) { 
        t.update(_moveX, _moveY);
      } 
      else {           
        t.update(0, 0);
      }
      t.draw();
    }

    // Eliminamos los trinagulos muertos
    for (int i = triangles.size()-1; i>=0; i--) {
      Triangle t = triangles.get(i);
      if (t.isDead()) {
        triangles.remove(i);
      }
    }
  }

  // Clase que representa a un triangulos
  class Triangle {
    
    PVector p1, p2, p3;
    PVector position;
    color c;
    float alpha;
    boolean dying;

    // crea una nueva instancia de triangulo
    public Triangle() {
      // los tres puntos del triangulo
      this.p1 = new PVector(random(0, 300), random(0, 300));
      this.p2 = new PVector(random(0, 300), random(0, 300));
      this.p3 = new PVector(random(0, 300), random(0, 300));
      // La posicion
      this.position = new PVector(random(-100, w+100), random(-100, h+100));
      // opacidad i vida
      this.alpha = 0;
      this.dying = false;
    }

    // Establece el color del triangulo
    public void setColor(color c) {
      this.c = c;
    }

    // Dibujamos el triangulo
    public void draw() {
      pushMatrix();
      {
        translate(this.position.x, this.position.y);
        stroke(this.c, this.alpha+30);
        fill(this.c, this.alpha);
        triangle(this.p1.x, this.p1.y, this.p2.x, this.p2.y, this.p3.x, this.p3.y);
      }
      popMatrix();
    }

    // actualiza el triangulo
    public void update(int _x, int _y) {
      
      // La posicion
      this.position.x += _x;
      this.position.y += _y;

      // la opacidad
      if (this.alpha >= 40) {
        this.dying = true;
      }

      this.alpha += (this.dying ? (-0.5) :0.5);
    }

    // Indica si el triangulo ha muerto
    public boolean isDead() {
      return this.alpha <= 0;
    }

  }
}


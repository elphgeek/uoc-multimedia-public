
// Contenedor para las variables
public class Variables {

  // Color de fondo
  color background; 

  // Paleta de colores para la escena
  color[] colors;

  // Volumen
  int volumen = 0;
  
  // Beat
  boolean beatDebug = false;
  BeatDetect beat;
  DebugBeatDetect debugBeat = new DebugBeatDetect();


  // Meteorologia
  int w_code= 0;
  int w_temp = 0;
  float w_windspeed = 0;
  int w_winddir = 0;

   
  // Establece los valores meteorologicos (si son validos)
  public void setWeather(int condition_code, int out_temperature, float wind_speed, int wind_direction) {
    if(condition_code > -1) this.w_code = condition_code;  
    if(out_temperature > -1) this.w_temp = out_temperature;
    if(wind_speed > -1) this.w_windspeed = wind_speed;
    if(wind_direction > -1) this.w_winddir = wind_direction;
  }

  // Obtiene la direccion del viento (grados 0-360)
  public int getWindDirection() {
    return this.w_winddir;
  }
  
  // Obtiene la velocidad del viento (kmph)
  public float getWinSpeed() {
    return this.w_windspeed;
  }

  // Obtiene un valor si llueve o no
  public boolean getIsRaining() {
    return (this.getRainingLevel() > 0);
  }

  // Obtiene el nivel de lluvia
  public int getRainingLevel() {
    switch(this.w_code)
    {
    case 0://  tornado
      return 5;
    case 1://  tropical storm
      return 5;
    case 2://  hurricane
    case 3://  severe thunderstorms
      return 5;
    case 4://  thunderstorms
      return 4;
    case 5://  mixed rain and snow
    case 6://  mixed rain and sleet
    case 7://  mixed snow and sleet
      return 3;
    case 8://  freezing drizzle
    case 9 :// drizzle
      return 1;
    case 10 :// freezing rain
    case 11 :// showers
    case 12://  showers
      return 2;
    case 37 :// isolated thunderstorms
    case 38 :// scattered thunderstorms
    case 39://  scattered thunderstorms
    case 40://  scattered showers
      return 3;
    case 45://  thundershowers
    case 46 :// snow showers
    case 47 :// isolated thundershowers
      return 4;
    default:
      return 0;
    }
  }

  // establece la hora del dia
  public void setHourOfDay(int v) {
    if (v < 6 || v > 21)
      this.background = color(0);
    else {
      int c = (int)map(v, 6, 21, 0, 50);
      this.background = color(c);
    }
  }
  
  // Establece la paleta a usar dependiendo de la estacion
  public void setSeason(int season) {
    switch(season) {
    case 0:
      colors = winter_palette;
      break;
    case 1:
      colors = spring_palette;
      break;
    case 2:
      colors = summer_palette;
      break;
    default:
      colors = autum_palette;
      break;
    }
  }

  // Obtiene un color aleatorio de la paleta actual
  public color getRandomColor() {
    color r = (color)this.colors[(int)random(this.colors.length)];
    return r;
  }

  // Obtiene el color de fondo
  public color getBgColor() {
    return this.background;
  }

  // Establece el valor del volument
  public void setVolumen(int v) {
    this.volumen = v;
  }

  // obtiene el valor del volumen
  public int getVolumen() {
    return this.volumen;
  }

  // Obtiene el Beat
  public BeatDetect getBeat() {
    return this.beatDebug ?  this.debugBeat : beat;
  }

  // Establece el Beat de depuracion, ya que el beat es algo mas complejo que un valor quantitativo normal
  public void setDebugBeat(boolean v) {
    this.debugBeat.set(v);
    this.beatDebug = v;
  }

  // Establece el Beat
  public void setBeat(BeatDetect beat) {
    this.beat = beat;
  }

  // CONSTANTS

  private color[] summer_palette = { //define colors here
    //http://www.colourlovers.com/palette/1473/Ocean_Five
    color(0, 160, 176), color(106, 74, 60), color(204, 51, 63), color(235, 104, 65), color(237, 201, 81), 
    //http://www.colourlovers.com/palette/92095/Giant_Goldfish
    color(105, 210, 231), color(167, 219, 216), color(224, 228, 204), color(243, 134, 48), color(250, 105, 0), 
    // http://www.colourlovers.com/palette/971420/Hippie_van_%E2%98%AE
    color(253, 230, 189), color(161, 197, 171), color(244, 221, 81), color(209, 30, 72), color(99, 47, 83)
  };

  private color[] winter_palette = { //define colors here
    //http://www.colourlovers.com/palette/1662417/h_a_n_a_i_r_o
    color(248, 237, 205), color(228, 226, 201), color(186, 198, 176), color(91, 57, 48), color(125, 106, 102), 
    //http://www.colourlovers.com/palette/1746404/Winter_Lover_Land
    color(219, 217, 183), color(193, 201, 200), color(165, 181, 171), color(148, 154, 142), color(97, 85, 102), 
    //http://www.colourlovers.com/palette/1898250/A_Kiss_for_Snowman
    color(72, 72, 70), color(190, 73, 99), color(180, 141, 76), color(171, 202, 205), color(242, 241, 237)
  };

  private color[] spring_palette = { //define colors here
    //http://www.colourlovers.com/palette/962376/I_like_your_Smile
    color(179, 204, 87), color(236, 240, 129), color(255, 190, 64), color(239, 116, 111), color(171, 62, 91), 
    //http://www.colourlovers.com/palette/56122/Sweet_Lolly
    color(255, 0, 60), color(255, 138, 0), color(250, 190, 40), color(136, 193, 0), color(0, 193, 118), 
    //http://www.colourlovers.com/palette/1599875/s_p_r_i_n_g_t_i_m_e
    color(255, 79, 114), color(255, 120, 154), color(235, 159, 155), color(222, 184, 160), color(209, 203, 169)
  };

  private color[] autum_palette = { //define colors here
    //http://www.colourlovers.com/palette/1960035/l_e_a_v_e_s
    color(247, 137, 48), color(178, 123, 67), color(174, 134, 99), color(159, 155, 103), color(158, 168, 132), 
    //http://www.colourlovers.com/palette/29411/Autumn_Oak
    color(68, 68, 68), color(252, 247, 209), color(169, 161, 122), color(181, 44, 0), color(140, 0, 5), 
    //http://www.colourlovers.com/palette/953498/Headache
    color(101, 86, 67), color(128, 188, 163), color(246, 247, 189), color(230, 172, 39), color(191, 77, 40)
  };
}

// Clase de utilidad para simular Beats en el modo depuracion
class DebugBeatDetect extends BeatDetect {
  boolean _v;
  public void set(boolean v) {
    _v = v;
  }
  public boolean isKick() {
    return false;
  } 

  public boolean isHat() {
    return false;
  }

  public boolean isSnare() {
    return false;
  }

  public boolean isRange(int x, int y, int z) {
    boolean __v = _v;
    set(false);
    return __v;
  }
}


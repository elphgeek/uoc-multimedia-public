import com.onformative.yahooweather.*;


// Objeto que ctualiza las variables con los datos externos
class World {

  // Where on earth id
  int _woeid = 0;
  // Tiempo entre actualizaciones
  int UPDATE_INTERVAL = 120000;
  // Contenedor del servicio
  YahooWeather _weather; 

  // Minin                     
  Minim _minim;
  // Entrada de audio
  AudioInput _in;
  // Detecor de beats
  BeatDetect _beat;  
  // Listener, para no perder informacion
  BeatListener _bl;

  // Flag para marcar como modo depuracion
  boolean _isDebugModeOn = false;
  // Coleccion de variables
  Variables _vars;

  // Crea una nueva instancia del Mundo
  public World(PApplet applet, int woeid) {
    // Inicializamos las variables
    _vars = new Variables();
    
    // Inicializamos el servidio de Yahoo 
    _woeid = woeid;
    _weather = new YahooWeather(applet, _woeid, "c", UPDATE_INTERVAL);
    

    // Inicializamos el analisis de audio
    _minim = new Minim(this);
    _in = _minim.getLineIn(Minim.STEREO, 512);         //get the micro line in
    _beat = new BeatDetect(_in.bufferSize(), _in.sampleRate());
    _beat.setSensitivity(300);  
    _bl = new BeatListener(_beat, _in);
    _vars.beat = _beat;
  }

  // aqui se actualizaran los datos de las variables, solo si no se esta en modo depuracion,
  // en cuyo caso los datos de las variables se sobreescribiran en GUI
  void draw() {

    // Actualizamos el tiempo i modificamos las variables
    _weather.update();
    if (!this._isDebugModeOn) {
      // Codigos de Condicion
      // http://developer.yahoo.com/weather/#codes
      int condition_code = _weather.getWeatherConditionCode();  
      int out_temperature = _weather.getTemperature();
      // velocidad del viento en kmph
      float wind_speed = _weather.getWindSpeed();
      // Direccion del viento en grados 0-360
      int wind_direction = _weather.getWindDirection();
      _vars.setWeather(condition_code, out_temperature, wind_speed, wind_direction);
    }

    // Obtenemos la fecha i hora actual, que nos servira para la hora del dia i la estacion del año
    Date _now = new Date();
    int month = _now.getMonth();
    int season = 0;
    if (month >= 11 && month <= 1) season = 0;        // Winter
    else if (month >= 2 && month <= 4) season = 1;    // Spring
    else if (month >= 5 && month <= 7) season = 2;    // Summer 
    else season = 3;                                  // Fall

    // nos guardamos la estacion
    if (!this._isDebugModeOn) {
      _vars.setSeason(season);
    }

    // Obtenemos la hora del dia
    Calendar calendar = GregorianCalendar.getInstance();
    calendar.setTime(_now);                                 // assigns calendar to given date 
    if (!this._isDebugModeOn) {
      _vars.setHourOfDay(calendar.get(Calendar.HOUR_OF_DAY)); // gets hour in 24h format
    }

    // Analisi de audio.
    // Usamos minim para obtener el nivel de volumen (0-1)i lo mapeamos a 0-255
    // Usamos ambos canales
    int volumen = (int)map(_in.mix.level(), 0, 1, 0, 255);
    if (!this._isDebugModeOn) {
      _vars.setVolumen(volumen);
    }
   
    // Guardamos el Beat 
    if(!this._isDebugModeOn){
       _vars.setDebugBeat(false);
       _vars.setBeat(_beat); 
    }
  }

  // Para los recursos en uso
  public void stop() {
    _minim.stop();
  }

  // Establece el modo de depuracion
  public void setDebugMode(boolean debug) {
    this._isDebugModeOn = debug;
  }

  // Obtiene las Variables
  public Variables getVariables() {
    return _vars;
  }
}


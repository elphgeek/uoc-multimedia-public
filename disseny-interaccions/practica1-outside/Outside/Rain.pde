
// Representación visual de la lluvia
// La lluvia esta formada por un conjunto de gotas, que sufren los efectos de las fuerzas de la gravedad i del viento
// El nivell de lluvia esta representado con la cantidad de gotas que se crean
class Rain {

  ArrayList<Drop> drops = new ArrayList<Drop>();
  int level = 0;
  int direction = 0;
  float speed = 0;

  PVector wind = new PVector(0, 0);
  PVector gravity = new PVector(0, 0.5 );

  // Instancia la lluvia
  public Rain(Variables vars) {
    this.update(vars);
  }

  // Actualiza la lluvia con las variables del Mundo
  public void update(Variables vars) {
    this.level = vars.getRainingLevel();
    this.direction = vars.getWindDirection();
    this.speed = vars.getWinSpeed();

    // Creamos el vector que representa la fuerza del viento, dependiendo de la direccion i de la velocidad
    if (direction > 180)
      this.wind = new PVector(map(speed, 0, 50, 0, 0.5), 0);
    else if (direction < 180)
      this.wind = new PVector(-map(speed, 0, 50, 0, 0.5), 0);
  }

  // Dibujamos la lluvia
  public void draw(Variables vars) {

    // Creamos las gotas
    for (int i = 0; i < level*3;i++) {
      drops.add(new Drop());
    }

    // iteramos por las gotas, les aplicamos las fuerzaas i las dibujamos
    for (int i = 0; i<drops.size(); i++) {
      Drop d = drops.get(i);

      d.applyForce(wind);
      d.applyForce(gravity);

      d.update();
      d.draw();
    }

    // Eliminar aquellas gotas que estan fuera de la pantalla
    for (int i = drops.size()-1; i>=0; i--) {
      Drop d = drops.get(i);
      if (d.getIsOutside()) {
        drops.remove(i);
      }
    }
  }
}

// Clase para las Gotas.
class Drop {
  PVector p1, p2;
  PVector position, acceleration, velocity;
  float mass;

  // creamos una nueva goa, en una posicion concreta i con una masa mas o menos aleatoria
  public Drop() {
    // Tenemos en cuenta que la fuerza del viento puede hacer que las gotas se dirijan a un lado, asi
    // que distribuimos las mismas por un segmento mayor que el de la pantalla (por si acaso)
    this.position = new PVector(random(-displayWidth, displayWidth*2), random(-0, -100));
    this.acceleration = new PVector(0, 0);
    this.velocity = new PVector(0, 0);
    this.mass = random(3, 7);
  }

  // Aplicamos las fuerzas a la gota
  public void applyForce(PVector force) {
    PVector f = force.get();
    f.div(mass);
    this.acceleration.add(f);
  }

  // Actualizamos la gota, aumentando la velocidad i cambiando la posicion
  public void update() {
    this.velocity.add(acceleration);
    this.position.add(velocity);
    this.acceleration.mult(0);
  }

  // Dibujamos la gota
  public void draw() {
    pushMatrix();
    {
      translate(this.position.x, this.position.y, -10);
      pushStyle();
      stroke(220, 228, 255, 40);
      noFill();
      // hacemos que la linea varie un poco para darle un poco mas de vida
      line(0, 0, 0, this.mass*random(5, 10));
      popStyle();
    }
    popMatrix();
  }

  // Comprueba si la gota esta fuera de la pantalla ( con cierto margen para reducir los efectos del viento)
  public boolean getIsOutside() {
    return ((this.position.x < (-displayWidth/3) || this.position.x > displayWidth +(displayWidth/3) ) || 
      (this.position.y > displayHeight));
  }
}

